import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpsComponent } from './components/https.component';

const routes: Routes = [
  {
    path:"https",
    component:HttpsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
