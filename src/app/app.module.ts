import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpsComponent } from './components/https.component';
import { httpInterceptProvider } from './interceptors';


@NgModule({
  declarations: [
    AppComponent,
    HttpsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [httpInterceptProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
