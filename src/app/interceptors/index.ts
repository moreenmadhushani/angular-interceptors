import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Httpsinterceptor } from './httpsinterceptor';

export const httpInterceptProvider = [
    { provide: HTTP_INTERCEPTORS, useClass: Httpsinterceptor, multi: true }
]
