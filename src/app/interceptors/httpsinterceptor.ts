import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class Httpsinterceptor implements HttpInterceptor {


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.url.includes('todos/1')) {
            return next.handle(req);
        }

        console.warn('http interceptors')

        const httpReq = req.clone({
            url: req.url.replace("http://", "https://")
        })

        return next.handle(httpReq);

    }
}