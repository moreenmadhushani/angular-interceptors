import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  template: `
  <h3>Response</h3>
  <pre></pre>
  <button color="primary" (click)="request()">
    Run
  </button>
`
})
export class HttpsComponent implements OnInit {

  response: Observable<any>;
  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {

  }

  request() {
    const url = "http://jsonplaceholder.typicode.com/todos/1";
    this.http.get(url).subscribe(
      data => console.log(data)
    )
  }

}
